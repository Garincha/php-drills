<?php
    //ini-set("display_errors","On");
    include_once('../vendor/autoload.php');
    //include("../src/institute.php"); including a class file like this way is not a standard way. 
    //instanciation - creating an object.
    // $myInstitute = new \Pondit\Institute;//  finding the class Institute through the namespace Pondit
    use Pondit\Institute;//  finding the class Institute through the namespace Pondit
    $myInstitute = new Institute;
     //var_dump($myInstitute);// seeing the inside of a variable
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index File</title>
</head>
<body>
<?php 
echo $myInstitute->name;  //accessing a property of an object
?>
</body>
</html>