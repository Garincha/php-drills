<?php
        //echo "<pre>";
        //print_r($_POST);
        //echo "</pre>";

        if(empty($_POST)){
            echo "User don't have any Hobby.";
            return;
        }

        $chosen_hobbies = [];
        if(array_key_exists('reading-novel',$_POST)){
            $chosen_hobbies[] = $_POST['reading-novel'];
        }
        if(array_key_exists('watching-movie',$_POST)){
            $chosen_hobbies[] = $_POST['watching-movie'];
        }
        if(array_key_exists('gardening',$_POST)){
            $chosen_hobbies[] = $_POST['gardening'];
        }
        if(array_key_exists('swimming',$_POST)){
            $chosen_hobbies[] = $_POST['swimming'];
        }else{
            //echo "User didn't choose any option.";
        }

        echo "User's chosen hobby(ies) is(are)".implode(", ",$chosen_hobbies).".";
    /*
    echo "<pre>";
        print_r($_POST);
    echo "</pre>";
  
    if(empty($_POST)){
        echo "User don't have any Hobby.";
        return;
    }
    
    if(array_key_exists('reading-novel',$_POST)){
            echo "User's Hobby is ".$_POST['reading-novel']."<br>";
    }
    if(array_key_exists('watching-movie',$_POST)){
        echo "User's Hobby is ".$_POST['watching-movie']."<br>";
    }
    if(array_key_exists('gardening',$_POST)){
        echo "User's Hobby is ".$_POST['gardening']."<br>";
    }
    if(array_key_exists('swimming',$_POST)){
        echo "User's Hobby is ".$_POST['swimming']."<br>";
    }else{
        //echo "User didn't choose any option.";
    }
      */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Alternative Code by foreach loop</title>
</head>
<body>
    <ul>
    <?php
        foreach($chosen_hobbies as $hobby){
            echo "<li>".$hobby."</li>";
        }
    ?>
    </ul>
</body>
</html>