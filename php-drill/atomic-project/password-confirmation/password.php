<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Component - Password Confirmation</title>
</head>
<body>
    <form action="processor.php" method="post">
        <div>
            <label for="password">
                <input type="password" id="password" name="password" placeholder="Enter your password">
                <button type="submit">Submit</button>
            </label>
        </div>
    </form>
</body>
</html>