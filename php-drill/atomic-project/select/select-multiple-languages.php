<?php
    $languages = [];

    if (array_key_exists('languages',$_POST)) {
        $languages = $_POST['languages'];
    }
    /*
    echo"<pre>";
    print_r($_POST['languages']);
    echo"</pre>";
    */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Multiple Options with multiple selection using HTML5 SELECT Tag</title>
</head>
<body>
    <h3>Languages that the user talk with</h3>
    <p><?php 
        if(count($languages) == 0){
            echo "User hasn't chosen any language.";
        }
    ?></p>
    <ul>
        <?php foreach($languages as $language):?>
        <li><?=$language;?></li>
    <?php endforeach; ?>
    </ul>
</body>
</html>