<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Component - Email</title>
</head>
<body>
    <form action="processor.php" method = "post">
        <div>
            <label for="email" >
                <input type="text" placeholder="Enter your email address" id="email" name="email" >
            </label>
            <button type="submit">SUBSCRIBE</button>
        </div>
    </form>
</body>
</html>