<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Concept</title>
</head>
<body>
    <form action="">
        <div>
            <label for="first_name">Enter First Name</label>
            <input type="text" id="first_name" name="first_name">
        </div>
        <div>
            <label for="last_name">Enter Last Name</label>
            <input type="text" id="last_name" name="last_name">
        </div>
        <button type="submit">Save</button>
    </form>
    
</body>
</html>

<?php
    
    $first_name = $_GET['first_name'];// printing the value of first_name by &_GET Method
    $last_name = $_GET['last_name'];
    $full_name = 'Your Full Name : '.$first_name.$last_name;
    echo $full_name;
?>