<?php
    //Ex 5.
//PHP Variable: case sensitive
//In PHP, variable names are case sensitive. So variable name "color" is different from Color, COLOR, COLor etc.

$color="red";
echo "My car is " . $color . "<br>";
echo "My house is " . $COLOR . "<br>";
echo "My boat is " . $coLOR . "<br>";
echo "<br>";

?>