<?php
    $text   = "\t\tThese are a few words :) ...  ";
    $binary = "\x09Example string\x0A";
    $hello  = "Hello World";
    var_dump($text, $binary, $hello);
    echo"<br>";
    
    //print "\n";
    
    $trimmed = trim($text);
    var_dump($trimmed);
    echo "<br>";
    
    $trimmed = trim($text, " \t.");
    var_dump($trimmed);
    echo "<br>";
    
    $trimmed = trim($binary,"Exa");
    var_dump($trimmed);
    echo "<br>";

    $trimmed = trim($hello, "Hdle");
    var_dump($trimmed);
    echo "<br>";

    $trimmed = trim($hello, 'HdWr');
    var_dump($trimmed);
    echo "<br>";
    // trim the ASCII control characters at the beginning and end of $binary
    // (from 0 to 31 inclusive)
    $clean = trim($binary, "\x00..\x1F");
    var_dump($clean);
    
?>