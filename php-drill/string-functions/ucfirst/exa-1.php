<?php
    $foo = 'hello world!';
    $foo = ucfirst($foo);             // Hello world!
    echo $foo."<br>";

    $bar = 'HELLO WORLD!';
    $bar = ucfirst($bar);             // HELLO WORLD!
    echo $bar."<br>";
    $bar = ucfirst(strtolower($bar)); // Hello world!
    echo $bar;
?>