<?php
    $rest = substr("abcdef", -1);    // returns "f"
    echo $rest."<br>";
    $rest2 = substr("abcdef", -2);    // returns "ef"
    echo $rest2."<br>";
    $rest3 = substr("abcdef", -3, 1); // returns "d"
    echo $rest3;
?>