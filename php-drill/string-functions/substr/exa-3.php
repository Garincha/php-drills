<?php
    echo substr('abcdef', 1)."<br>";     // bcdef
    echo substr('abcdef', 1, 3)."<br>";  // bcd
    echo substr('abcdef', 0, 4)."<br>";  // abcd
    echo substr('abcdef', 0, 8)."<br>";  // abcdef
    echo substr('abcdef', -1, 1)."<br>"; // f
    
    // Accessing single characters in a string
    // can also be achieved using "square brackets"
    $string = 'abcdef';
    echo $string[0]."<br>";                 // a
    echo $string[3]."<br>";                 // d
    echo $string[strlen($string)-1]; // f
?>