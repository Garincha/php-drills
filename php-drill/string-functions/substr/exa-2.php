<?php
    /*
         length

    If length is given and is positive, the string returned will contain at most length characters beginning from start (depending on the length of string).

    If length is given and is negative, then that many characters will be omitted from the end of string (after the start position has been calculated when a start is negative). If start denotes the position of this truncation or beyond, FALSE will be returned.

    If length is given and is 0, FALSE or NULL, an empty string will be returned.

    If length is omitted, the substring starting from start until the end of the string will be returned. 
    */
    $rest = substr("abcdef", 0, -1);  // returns "abcde"
    echo $rest."<br>";
    $rest2 = substr("abcdef", 2, -1);  // returns "cde"
    echo $rest2."<br>";
    $rest3 = substr("abcdef", 4, -4);  // returns false
    echo $rest3."<br>";
    $rest4 = substr("abcdef", -3, -1); // returns "de"
    echo $rest4."<br>";
?>