<?php
    // We can search for the character, ignoring anything before the offset
    $newstring = 'abcdef abcdef';
    echo strpos($newstring, 'a', 1); // $pos = 7, not 0
?>