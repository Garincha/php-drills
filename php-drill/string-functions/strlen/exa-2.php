<?php
    $attributes = array('one', 'two', 'three');

    if (strlen($attributes) == 0 && !is_bool($attributes)) {
        echo "We are in the 'if'\n";  //  PHP 5.3
    } else {
        echo "We are in the 'else'\n";  //  PHP 5.2
    }
?>