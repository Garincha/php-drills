<?php
    function destroy_foo() 
    {
        global $foo ;
        $foo = "I'm Global";
        unset($foo);
    }
    
    $foo = "I'm Local";
    destroy_foo();
    echo $foo;

    /*
        unset() destroys the specified variables.

        The behavior of unset() inside of a function can vary depending on what type of variable you are attempting to destroy.

        If a globalized variable is unset() inside of a function, only the local variable is destroyed. The variable in the calling environment will retain the same value as before unset() was called.
    */
?>