<?php
    //Using (unset) casting

    $name = 'Felipe';

    var_dump((unset) $name);
    echo"<br>";
    var_dump($name);

    /*
        (unset) casting is often confused with the unset() function. 
        (unset) casting serves only as a NULL-type cast, for completeness. 
        It does not alter the variable it's casting. The (unset) cast is deprecated as of PHP 7.2.0.
    */
?>