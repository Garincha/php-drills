<?php
    // destroy a single variable
        $foo = "This is foo before unset";
        echo $foo;
        unset($foo);
        echo $foo;

    // destroy a single element of an array
        $bar = array("one" => "an array", "quxx" => "this element will unset");
        print_r ($bar);
        echo"<br>";
        unset($bar['quux']);
        print_r ($bar);
        echo"<br>";

    // destroy more than one variable
        $foo1 = "This is foo1";
        $foo2 = "This is foo2";
        $foo3 = "This is foo3";
        echo $foo1, $foo2, $foo3;
        
        unset($foo1, $foo2, $foo3);

        echo $foo1, $foo2, $foo3;
?>