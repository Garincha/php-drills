<?php
    function foo(&$bar) 
    {
        unset($bar);
        $bar = "blah";
    }
    
    $bar = 'something';
    echo "$bar\n";
    
    foo($bar);
    echo "$bar\n";

    /*
        If a variable that is PASSED BY REFERENCE is unset() inside of a function, 
        only the local variable is destroyed. The variable in the calling environment will retain the same 
        value as before unset() was called.
    */
?>