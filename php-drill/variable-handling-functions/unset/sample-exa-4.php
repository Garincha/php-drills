<?php
    function foo()
    {
        static $bar;
        $bar++;
        echo "Before unset: $bar, ";
        unset($bar);
        $bar = 23;
        echo "after unset: $bar\n";
    }
    
    foo();
    foo();
    foo();

    /*
        If a static variable is unset() inside of a function, unset() destroys the variable only in the 
        context of the rest of a function. Following calls will restore the previous value of a variable.
    */
?>