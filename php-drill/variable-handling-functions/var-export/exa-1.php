<?php
    //var_export — Outputs or returns a parsable string representation of a variable

    $a = array (1, 2, array ("a", "b", "c"));
    var_export($a);

    /*
        var_export() gets structured information about the given variable. 
        It is similar to var_dump() with one exception: the returned representation is valid PHP code.
    */
?>