<?php
    //is_callable — Verify that the contents of a variable can be called as a function

    class Foo
    {
        public function __construct() {}
        public function foo() {}
    }
    
    var_dump(
        is_callable(array('Foo', '__construct')),
        is_callable(array('Foo', 'foo'))
    );
?>