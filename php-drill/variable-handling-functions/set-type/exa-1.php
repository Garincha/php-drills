<?php
    //settype — Set the type of a variable

    $foo = "5bar"; // string
    $bar = true;   // boolean

    settype($foo, "integer"); // $foo is now 5   (integer)
    settype($bar, "string");  // $bar is now "1" (string)

    var_dump($foo);
    echo"<br>";
    var_dump($bar);
?>