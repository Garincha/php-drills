<?php
    echo 'this is a simple string';
    echo "<hr>\n";
    echo 'You can also have embedded newlines in
    strings this way as it is
    okay to do';
    echo "<hr>\n";
    // Outputs: Arnold once said: "I'll be back"
    echo 'Arnold once said: "I\'ll be back"';
    echo "<hr>\n";
    // Outputs: You deleted C:\*.*?
    echo 'You deleted C:\\*.*?';
    echo "<hr>\n";
    // Outputs: You deleted C:\*.*?
    echo 'You deleted C:\*.*?';
    echo "<hr>\n";
    // Outputs: This will not expand: \n a newline
    echo 'This will not expand: \n a newline';
    echo "<hr>\n";
    // Outputs: Variables do not $expand $either
    echo 'Variables do not $expand $either';
?>