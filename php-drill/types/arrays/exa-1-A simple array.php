<?php
    $array = array(
        "foo" => "bar",
        "bar" => "foo",
    );
    print_r($array);
    // as of PHP 5.4
    $array2 = [
        "foo" => "bar",
        "bar" => "foo",
    ];
    print_r($array2);
?>