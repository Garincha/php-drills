<?php
    // Arrays are compared like this with standard comparison operators
        function standard_array_compare($op1, $op2)
        {
            if (count($op1) < count($op2)) {
                return -1; // $op1 < $op2
            } elseif (count($op1) > count($op2)) {
                return 1; // $op1 > $op2
            }
            foreach ($op1 as $key => $val) {
                if (!array_key_exists($key, $op2)) {
                    return null; // uncomparable
                } elseif ($val < $op2[$key]) {
                    return -1;
                } elseif ($val > $op2[$key]) {
                    return 1;
                }
            }
            return 0; // $op1 == $op2
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #2 Transcription of standard array comparison</title>
</head>
<body>
    
</body>
</html>