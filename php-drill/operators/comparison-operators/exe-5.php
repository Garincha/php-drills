<?php
    // Example usage for: Ternary Operator
        $action = (empty($_POST['action'])) ? 'default' : $_POST['action'];

        // The above is identical to this if/else statement
        if (empty($_POST['action'])) {
            $action = 'default';
        } else {
            $action = $_POST['action'];
        }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #3 Assigning a default value</title>
</head>
<body>
    
</body>
</html>