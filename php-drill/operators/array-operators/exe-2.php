<?php
    $a = array("apple", "banana");
    $b = array(1 => "banana", "0" => "apple");
    
    var_dump($a == $b); // bool(true)
    var_dump($a === $b); // bool(false)
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #1 Comparing arrays</title>
</head>
<body>
    
</body>
</html>