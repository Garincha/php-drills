<?php
    $a = 1;
    echo $a + $a++; // may print either 2 or 3
    
    $i = 1;
    $array[$i] = $i++; // may set either index 1 or 2
    print_r($array);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #2 Undefined order of evaluation</title>
</head>
<body>
    
</body>
</html>