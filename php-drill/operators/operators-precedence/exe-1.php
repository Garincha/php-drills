<?php
    $a = 3 * 3 % 5; // (3 * 3) % 5 = 4
    // ternary operator associativity differs from C/C++
    $a = true ? 0 : true ? 1 : 2; // (true ? 0 : true) ? 1 : 2 = 2
    
    $a = 1;
    $b = 2;
    $a = $b += 3; // $a = ($b += 3) -> $a = 5, $b = 5
    echo $a;
    echo"<br>";
    echo $b;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #1 Associativity</title>
</head>
<body>
    
</body>
</html>