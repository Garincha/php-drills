<?php
    $x;
    // this line might result in unexpected output:
    echo "x minus one equals " . $x-1 . ", or so I hope\n";
    // because it is evaluated like this line:
    echo (("x minus one equals " . $x) - 1) . ", or so I hope\n";
    // the desired precedence can be enforced by using parentheses:
    echo "x minus one equals " . ($x-1) . ", or so I hope\n";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #3 +, - and . have the same precedence</title>
</head>
<body>
    
</body>
</html>