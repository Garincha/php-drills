<?php
    echo 12 ^ 9; // Outputs '5'

    echo "12" ^ "9"; // Outputs the Backspace character (ascii 8)
                     // ('1' (ascii 49)) ^ ('9' (ascii 57)) = #8
    
    echo "hallo" ^ "hello"; // Outputs the ascii values #0 #4 #0 #0 #0
                            // 'a' ^ 'e' = #4
    
    echo 2 ^ "3"; // Outputs 1
                  // 2 ^ ((int)"3") == 1
    
    echo "2" ^ 3; // Outputs 1
                  // ((int)"2") ^ 3 == 1
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #2 Bitwise XOR operations on strings</title>
</head>
<body>
    
</body>
</html>