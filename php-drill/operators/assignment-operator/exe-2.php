<?php
    $a = 3;
    $a += 5; // sets $a to 8, as if we had said: $a = $a + 5;
    echo $a."<br>";
    $b = "Hello ";
    $b .= "There!"; // sets $b to "Hello There!", just like $b = $b . "There!";
    echo $b;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
</body>
</html>