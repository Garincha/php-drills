<?php
    $a = 3;
    $b = &$a; // $b is a reference to $a
    
    print "$a\n"; // prints 3
    print "$b\n"; // prints 3
    
    $a = 4; // change $a
    
    print "$a\n"; // prints 4
    print "$b\n"; // prints 4 as well, since $b is a reference to $a, which has
                  // been changed
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #1 Assigning by reference</title>
</head>
<body>
    
</body>
</html>