<?php
    $a;
    $d = 'NotMyClass';
    var_dump($a instanceof $d); // no fatal error here
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #7 Avoiding classname lookups and fatal errors with instanceof in PHP 5.0</title>
</head>
<body>
    
</body>
</html>