<?php
    class MyClass
    {
    }
    
    class NotMyClass
    {
    }
    $a = new MyClass;
    
    var_dump($a instanceof MyClass);
    var_dump($a instanceof NotMyClass);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #1 Using instanceof with classes</title>
</head>
<body>
    
</body>
</html>