<?php
    interface MyInterface
    {
    }
    
    class MyClass implements MyInterface
    {
    }
    
    $a = new MyClass;
    
    var_dump($a instanceof MyClass);
    var_dump($a instanceof MyInterface);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #4 Using instanceof with interfaces</title>
</head>
<body>
    
</body>
</html>