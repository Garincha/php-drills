<?php
    interface MyInterface
    {
    }
    
    class MyClass implements MyInterface
    {
    }
    
    $a = new MyClass;
    $b = new MyClass;
    $c = 'MyClass';
    $d = 'NotMyClass';
    
    var_dump($a instanceof $b); // $b is an object of class MyClass
    var_dump($a instanceof $c); // $c is a string 'MyClass'
    var_dump($a instanceof $d); // $d is a string 'NotMyClass'
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #5 Using instanceof with other variables</title>
</head>
<body>
    
</body>
</html>