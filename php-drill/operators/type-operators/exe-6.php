<?php
    $a = 1;
    $b = NULL;
    $c = imagecreate(5, 5);
    var_dump($a instanceof stdClass); // $a is an integer
    var_dump($b instanceof stdClass); // $b is NULL
    var_dump($c instanceof stdClass); // $c is a resource
    //var_dump(FALSE instanceof stdClass);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #6 Using instanceof to test other variables</title>
</head>
<body>
    
</body>
</html>