<?php
    class ParentClass
    {
    }
    
    class MyClass extends ParentClass
    {
    }
    
    $a = new MyClass;
    
    var_dump($a instanceof MyClass);
    var_dump($a instanceof ParentClass);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Example #2 Using instanceof with inherited classes</title>
</head>
<body>
    
</body>
</html>