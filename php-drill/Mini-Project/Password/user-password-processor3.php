<?php

   //Password length check//
function isValidLength($password){        
    if(strlen($password) >= 8 ){
        return true;
    }
    return false;
}

function hasUppercase($password){
    if(preg_match("#[A-Z]#", $password)){
        return true;
    }
    return false;
}

function hasLowercase($password){
    if(preg_match("#[a-z]#", $password)){
         return true;  
    }
    return false;
}

function checkNumber($password){
    if(preg_match("#[0-9]#", $password)){
        return true; 
    }
    return false;
}

function checkSpecialCharacter($password){
    if(preg_match("#[^\w]#", $password)){
        return true; 
    }
    return false; 
}
function displayValidationErrors($errors){
    // TODO LOOP & DISPLAY ERRORS//
    echo "<ul>";
        foreach($errors as $error){
            echo "<li>".$error."</li>";
        }
        echo "</ul>";
    // echo '<pre>';
    //     print_r($errors);
    // echo '</pre>';
   
}

function validatePassword($password){
    $errors = [];
            if(!isValidLength($password)){
                $errors[]= "Your password doesn't meet accepted length";
            }
            if(!hasUppercase($password)){
                $errors[]= "Use capital letter for making high security";
            }

            if(!hasLowercase($password)){
                $errors[]= "Use lower letter for making high security";
            }

            if(!checkNumber($password)){
                $errors[]= "Use numbers for making high security";
            }

            if(!checkSpecialCharacter($password)){
                $errors[]= "Use special characters for making high security";
            }
    

    return $errors;

}

    if(strtoupper($_SERVER['REQUEST_METHOD'])== 'POST'){
        $password= null;
        if(array_key_exists('password', $_POST)){
            $password = $_POST['password']; 
        }

        $errors = validatePassword($password);
    
        if(count($errors)>0){
            displayValidationErrors($errors);
        }
    
    }
    
    
    
    
    
    // //Password uppercase validation check//
    
    
    // //Password lowercase validation check//
    
    
    // // Password Number Check//
    // if(!preg_match("#[0-9]#", $password)){
    //     $errors[]= "You have to use Numaric";
    // }
    
    
    // // Password Special Charecter Check//
    
    // $special_charecter = '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/';
    
    // if(!preg_match($special_charecter, $password)){
    //     $errors[]= "You have to use one special charecter atleast";
    // }
    // //Combination of lowercase,uppercase,number//
    
    // $uppercase = preg_match('#[A-Z]#', $password);
    // $lowercase = preg_match('#[a-z]#', $password);
    // $number = preg_match('#[0-9]#', $password);
    
    // if(!$uppercase || !$lowercase || !$number){
    //     $errors[]="you have to match lowercase , uppercase letter & number";
    // }


?>