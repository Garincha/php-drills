<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>Form9</title>
  </head>
  <?php
        $form = <<<'PONDIT_JOHN'
  <body>
    <img src="img/form9.jpg" />
    <form action="#" method="post">
      <div>
        <label for="family-name">Family name:</label>
        <input
          id="family-name"
          type="text"
          name="family-name"
          value=""
          required
          placeholder="family name"
        />
      </div>
      <br />
      <div>
        <label for="other-names">Other name:</label>
        <input
          id="other-names"
          type="text"
          name="other-names"
          value=""
          required
          placeholder="Other names"
        />
      </div>
      <br />
      <div>
        <label for="address">Address:</label>
        <input
          id="address"
          type="address"
          name="address"
          value=""
          required
          placeholder="Address"
        />
      </div>
      <br />
      <div>
        <label for="suburb">Suburb:</label>
        <input
          id="suburb"
          type="suburb"
          name="suburb"
          value=""
          required
          placeholder="vic(drop down menu)"
        />
      </div>
      <br />
      <div>
        <label for="state-territory">State/Territory</label>
      </div>

      <br />

      <div>
        <lebel>Course info on:</lebel>
      </div>
      <div>
        <label for="engineering">
          <input
            id="engineering"
            type="checkbox"
            name="engineering"
            value="engineering"
          />
          Engineering
        </label>
      </div>
      <div>
        <label for="it">
          <input id="it" type="checkbox" name="it" value="it" />
          It
        </label>
      </div>
      <div>
        <label for="law">
          <input id="law" type="checkbox" name="law" value="law" />
          Law
        </label>
      </div>
      <div>
        <label for="medicin">
          <input
            id="medicine"
            type="checkbox"
            name="medicine"
            value="medicine"
          />
          Medicine
        </label>
      </div>
      <div>
        <label for="nursing">
          <input id="nursing" type="checkbox" name="nursing" value="nursing" />
          Nursing
        </label>
      </div>
      <br />
      <div>
        <label>Degree:</label>
      </div>
      <div>
        <label for="diploma">
          <input id="diploma" type="radio" name="diploma" value="diploma" />
          Diploma
        </label>
      </div>
      <div>
        <label for="degree">
          <input id="degree" type="radio" name="degree" value="degree" />
          Degree
        </label>
      </div>
      <div>
        <label for="honours">
          <input id="honours" type="radio" name="honours" value="honours" />
          Honours
        </label>
      </div>
      <div>
        <label for="masters">
          <input id="masters" type="radio" name="masters" value="masters" />
          Masters
        </label>
      </div>
      <div>
        <label for="phd">
          <input id="phd" type="radio" name="phd" value="phd" />
          Phd
        </label>
      </div>

      <div>
        <label>Comments:</label>
        <textarea rows="5" cols="20"></textarea>
      </div>
      <br />
      <button type="submit">Submit</button>
      <button type="clear-from">Clear from</button>
    </form>
  </body>
PONDIT_JOHN;
?>
<?php
echo $form;
?>
</html>
