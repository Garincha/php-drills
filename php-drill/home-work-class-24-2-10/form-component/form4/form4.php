<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Form4</title>
</head>

<?php
        $form = <<<'PONDIT_JOHN'
<body>
    <div>
        <h2>Image</h2>
        <img src="img/form4.jpg">
    </div>
    <h1>HTML<<span>fieldset> form Attribute</h1>
    <form action="#" method="post">
        <p>Suggest article for video:</p>
        <fieldset>
            <legend>JAVA:</legend>
            <div>
                <label for="title">Title:</label>
                <input id="title" type="text" name="title" value="" required maxlength="25" placeholder="">
            </div>
            <div>
                <label for="link">Link:</label>
                <input id="link" type="text" name="link" value="" required maxlength="25" placeholder="">
            </div>
            <div>
                <label for="user-id">User id:</label>
                <input id="user-id" type="user-id" name="user-id" value="" required maxlength="25" placeholder="">
            </div>

        </fieldset>
        <fieldset>
            <legend>PHP:</legend>
            <div>
                <label for="title">Title:</label>
                <input id="title" type="text" name="title" value="" required maxlength="25" placeholder="">
            </div>
            <div>
                <label for="link">Link:</label>
                <input id="link" type="text" name="link" value="" required maxlength="25" placeholder="">
            </div>
            <div>
                <label for="user-id">User id:</label>
                <input id="user-id" type="user-id" name="user-id" value="" required maxlength="25" placeholder="">
            </div>

        </fieldset>
    </form>
</body>
PONDIT_JOHN;
?>
<?php
echo $form;
?>
</html>