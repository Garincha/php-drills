<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form7</title>
</head>
<?php
        $form = <<<'PONDIT_JOHN'
<body>
    <div>
        <img src="img/form7.jpg">
    </div>
    <h1>HTML form</h1>
    <form action="#" method="post">

        <div>
            <label for="first-name">First Name:</label>
            <input id="first-name" type="text" name="first-name" value="" required placeholder="">
        </div>
        <br>

        <div>
            <label for="last-name">Last Name:</label>
            <input id="last-name" type="text" name="last-name" value="" required placeholder="">
        </div>
        <br>
        <div>
            <label for="email">email:</label>
            <input id="email" type="email" name="email" value="" required placeholder="">
        </div>

        <br>

        <div>
            <label for="male">
                <input id="male" type="checkbox" name="gender" value="male">
                Male
            </label>
        </div>
        <div>
            <label for="female">
                <input id="female" type="checkbox" name="gender" value="female">
                Female
            </label>
        </div>
        <br>
        <button type="send">Send</button>
        <button type="reset">Reset</button>


</body>
PONDIT_JOHN;
?>
<?php
echo $form;
?>
</html>