<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Footer component</title>
</head>
<?php
        $footer = <<<'PONDIT_JOHN'
<body>
    <img src="img/footer7.jpg">
    <footer>
        <div id="top-footer">
            <div id="about us">
                <h3><a href="#">ABOUT US</a></h3>
            </div>
            <div id="products">
                <h3><a href="#">PRODUCTS</a></h3>
            </div>
            <div id="awards">
                <h3><a href="#">AWARDS</a></h3>
            </div>
            <div id="help">
                <h3><a href="#">HELP</a></h3>
            </div>
            <div id="contact">
                <h3><a href="#">CONTACT</a></h3>
            </div>
        </div>
        <div id="middle-footer">
            <div>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatern accusantium doloremque
                    laudantium,totam rem aperiam,eaque ipsa quae ab illo jjinvantore veritatis at quasi architecto
                    beatae vitae dicta sunt explicabo.</p>
            </div>
            <nav>
                <ul>
                    <li><a href="#">facebook</a></li>
                    <li><a href="#">twitter</a></li>
                    <li><a href="#">google plus</a></li>
                    <li><a href="#">linkedin</a></li>
                    <li><a href="#">instagram</a></li>
                    <li><a href="#">Pinterest</a></li>
                </ul>
            </nav>
        </div>
        <div id="bottom-footer">
            <p>&copy; 2018 Copyright: MDBootstrap.com</p>
        </div>
    </footer>
</body>
PONDIT_JOHN;
?>
<?php
echo $footer;
?>
</html>